import React from "react";
import Home from "./pages/GithubRepos/ReposList";
import "./app.scss";

function App() {
  return <Home />;
}

export default App;

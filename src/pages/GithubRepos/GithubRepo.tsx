import { useCallback, useRef } from "react";
import { Col, Row } from "react-bootstrap";

interface Props {
  id?: number;
  index: number;
  name: string;
  description: string;
  avatar_url: string;
  ref?: any;
  length: number;
  loading: boolean;
  setPage: React.Dispatch<React.SetStateAction<number>>;
}
const GithubRepo: React.FC<Props> = props => {
  const { description, name, avatar_url, index, length, loading, setPage } =
    props;
  const observer: any = useRef();
  const LastRepo = useCallback(
    node => {
      if (loading) return;
      if (observer.current) observer.current.disconnect();
      observer.current = new IntersectionObserver(entires => {
        if (entires[0].isIntersecting) {
          setPage(pr => pr + 1);
        }
      });
      if (node) observer.current.observe(node);
    },
    [loading, observer, setPage]
  );
  return (
    <>
      <div className="repo" ref={length === index + 1 ? LastRepo : null}>
        <Row>
          <Col md={4}>
            <img src={avatar_url} alt="repo_picture" className="img-fluid" />
          </Col>
          <Col md={8}>
            <h1>{name}</h1>
            <p>{description}</p>
            <button>stars number</button>
            <button>Issues number</button>
            <span>Submitted 30 days ago by Name</span>
          </Col>
        </Row>
      </div>
    </>
  );
};

export default GithubRepo;

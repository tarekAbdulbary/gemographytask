import { useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import { url } from "../../config/url";
import GithubRepo from "./GithubRepo";
import "./reposList.scss";

const ReposList = () => {
  const [repos, setRepos] = useState<any[]>([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState("");
  const [page, setPage] = useState(1);

  useEffect(() => {
    setLoading(true);
    fetch(`${url}&page=${page}`, {
      method: "GET",
      headers: { "Content-Type": "application/json" },
    })
      .then(res => res.json())
      .then(data => {
        if (data.items) {
          setLoading(false);
          setRepos([...repos, ...data.items]);
        }
      })
      .catch(err => setError(err));
  }, [page, repos]);

  const repo =
    repos &&
    repos.map((repo: any, index: number) => {
      const {
        id,
        name,
        description,
        owner: { avatar_url },
      } = repo;
      const props = {
        id,
        name,
        description,
        avatar_url,
        index,
        length: repos.length,
        loading,
        setPage,
      };
      return <GithubRepo key={repo.id} {...props} />;
    });

  return (
    <>
      <section className="github_repos">
        <Container>
          {loading && <div>Loading...</div>}
          {error && <div>Some error occurred {error}</div>}
          {repo}
        </Container>
      </section>
    </>
  );
};

export default ReposList;
